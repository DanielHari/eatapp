package com.example.daniel.aplikacija1.Model;

import java.util.List;

public class Request {

    private String phone;
    private  String name;
    private String address;
    private String total;
    private  String status;
    private String Comment;
    private String paymentState;
    private String pamentMethod;
    //private String latLng;
    private List<Order> foods; // list of food order


    public Request() {
    }

    public Request(String phone, String name, String address, String total, String status, String comment, String paymentState, String pamentMethod,  List<Order> foods) {
        this.phone = phone;
        this.name = name;
        this.address = address;
        this.total = total;
        this.status = status;
        Comment = comment;
        this.paymentState = paymentState;
        this.pamentMethod = pamentMethod;

        this.foods = foods;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public String getPaymentState() {
        return paymentState;
    }

    public void setPaymentState(String paymentState) {
        this.paymentState = paymentState;
    }

    public String getPamentMethod() {
        return pamentMethod;
    }

    public void setPamentMethod(String pamentMethod) {
        this.pamentMethod = pamentMethod;
    }



    public List<Order> getFoods() {
        return foods;
    }

    public void setFoods(List<Order> foods) {
        this.foods = foods;
    }
}
