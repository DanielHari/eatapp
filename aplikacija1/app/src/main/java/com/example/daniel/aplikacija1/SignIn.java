package com.example.daniel.aplikacija1;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.daniel.aplikacija1.Common.Common;
import com.example.daniel.aplikacija1.Model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import io.paperdb.Paper;

public class SignIn extends AppCompatActivity {

    EditText edtPhone,edtPassword;
    Button btnSingIn;
    CheckBox checkBox;
    TextView txtForgotpwd;
    FirebaseDatabase database;
    DatabaseReference Table_User;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);


        edtPassword = (MaterialEditText)findViewById(R.id.edtpassword);
        edtPhone = (MaterialEditText)findViewById(R.id.edtPhone);
        btnSingIn = (Button)findViewById(R.id.btnSignIn);

        checkBox = (CheckBox)findViewById(R.id.CheckBox);
        txtForgotpwd = (TextView)findViewById(R.id.txtForgotPassword);

        Typeface face = Typeface.createFromAsset(getAssets(),"fonts/FONT.TFF");
        btnSingIn.setTypeface(face);
        edtPassword.setTypeface(face);
        edtPhone.setTypeface(face);

        //Init Paper
        Paper.init(this);

        //init Firebase

        database = FirebaseDatabase.getInstance();
        Table_User = database.getReference("User");

        txtForgotpwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showForgotPwdDialog();
            }
        });


        btnSingIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Common.isConnectedToInternet(getBaseContext())) {

                    //Save user & password

                    if(checkBox.isChecked()){

                        Paper.book().write(Common.USER_KEY,edtPhone.getText().toString());
                        Paper.book().write(Common.PWD_KEY,edtPassword.getText().toString());


                    }

                    final ProgressDialog mDialog = new ProgressDialog(SignIn.this);
                    mDialog.setMessage("Prosim počakajte..");
                    mDialog.show();

                    Table_User.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            //check if user obstaja


                            if (dataSnapshot.child(edtPhone.getText().toString()).exists()) {


                                //get info user

                                mDialog.dismiss();
                                User user = dataSnapshot.child(edtPhone.getText().toString()).getValue(User.class);
                                user.setPhone(edtPhone.getText().toString());//set phone
                                if (user.getPassword().equals(edtPassword.getText().toString())) {

                                    Intent homeIntent = new Intent(SignIn.this, Home.class);
                                    Common.currentUser = user;
                                    startActivity(homeIntent);
                                    finish();

                                    Table_User.removeEventListener(this);


                                } else {


                                    Toast.makeText(SignIn.this, "Prijava Neuspešna", Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                mDialog.dismiss();
                                Toast.makeText(SignIn.this, "Uporabnik ne obstaja", Toast.LENGTH_SHORT).show();

                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }
                else
                    Toast.makeText(SignIn.this, "Preveri povezavo!", Toast.LENGTH_SHORT).show();
                return;
            }
        });
    }

    private void showForgotPwdDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Pozabljeno Geslo");
        builder.setMessage("Vnesi zaščitno kodo: ");

        LayoutInflater inflater = this.getLayoutInflater();
        View forgo_view = inflater.inflate(R.layout.forgot_password_layout,null);

        builder.setView(forgo_view);
        builder.setIcon(R.drawable.ic_security_black_24dp);

        final MaterialEditText edtPhone = (MaterialEditText)forgo_view.findViewById(R.id.edtPhone);
        final MaterialEditText edtSecureCode = (MaterialEditText)forgo_view.findViewById(R.id.edtSecureCode);

        builder.setPositiveButton("DA", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //Check if user available
                Table_User.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.child(edtPhone.getText().toString())
                                .getValue(User.class);



                        if(dataSnapshot.child(edtPhone.getText().toString()).exists()){





                        if(user.getSecureCode().equals(edtSecureCode.getText().toString()))
                            Toast.makeText(SignIn.this, "Vaše geslo: "+user.getPassword(), Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(SignIn.this, "Napačna varnostna koda!!", Toast.LENGTH_SHORT).show();}
                        else{

                            Toast.makeText(SignIn.this, "Vnesi pravilno številko!", Toast.LENGTH_SHORT).show();
                        }



                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        });
        builder.setNegativeButton("NE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.show();


    }
}
