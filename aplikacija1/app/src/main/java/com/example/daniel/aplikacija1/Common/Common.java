package com.example.daniel.aplikacija1.Common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.daniel.aplikacija1.Model.User;

public class Common {

    public static User currentUser;

    public static final String INTENT_FOOD_ID = "foodId";

    public static String convertCodeToStatus(String status) {

        if(status.equals("0"))
            return "V teku";
        else if (status.equals("1"))
            return "Na poti";
        else
            return "Prispelo";
    }

    public static final String DELETE = "Izbriši";

        public static final String USER_KEY = "User";
    public static final String PWD_KEY = "Password";

    public static boolean isConnectedToInternet(Context context){


        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if(connectivityManager != null)
        {

            NetworkInfo[] info = connectivityManager.getAllNetworkInfo();

            if(info != null)
            {
                for (int i=0;i<info.length;i++){

                    if(info[i].getState() == NetworkInfo.State.CONNECTED)
                        return true;

                }

            }

        }
        return false;

    }
}
