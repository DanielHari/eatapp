package com.example.daniel.aplikacija1.viewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.daniel.aplikacija1.Interface.ItemClickListener;
import com.example.daniel.aplikacija1.R;

public class FavoritesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView food_name,food_price;
    public ImageView food_image,fav_image,share_image,quick_cart;

    private ItemClickListener itemClickListener;

    public LinearLayout view_foregroung;

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public FavoritesViewHolder(@NonNull View itemView) {
        super(itemView);

        food_name = (TextView)itemView.findViewById(R.id.food_name);
        food_image = (ImageView)itemView.findViewById(R.id.food_image);
        fav_image = (ImageView)itemView.findViewById(R.id.fav);
        share_image = (ImageView)itemView.findViewById(R.id.btnShare);
        food_price = (TextView)itemView.findViewById(R.id.food_price);
        quick_cart = (ImageView)itemView.findViewById(R.id.btn_quick_Cart);
        view_foregroung = (LinearLayout)itemView.findViewById(R.id.view_foreground);

        itemView.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        itemClickListener.OnClick(v,getAdapterPosition(),false);

    }
}