package com.example.daniel.aplikacija1.Interface;

import android.view.View;

public interface ItemClickListener {

    void OnClick(View view,int position,boolean isLongClick);
}
