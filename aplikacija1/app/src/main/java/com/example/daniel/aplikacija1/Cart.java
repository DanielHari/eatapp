package com.example.daniel.aplikacija1;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.daniel.aplikacija1.Common.Common;
import com.example.daniel.aplikacija1.Common.Config;
import com.example.daniel.aplikacija1.Database.Database;
import com.example.daniel.aplikacija1.Model.Order;
import com.example.daniel.aplikacija1.Model.Request;
import com.example.daniel.aplikacija1.viewHolder.CartAdapter;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import info.hoang8f.widget.FButton;

public class Cart extends AppCompatActivity {

    private static final int PAYPAL_REQUEST_CODE = 9999;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    FirebaseDatabase database;
    DatabaseReference requests;

   public TextView txtTotalPrice;

    FButton btnPlace;

   List<Order> cart = new ArrayList<>();

    CartAdapter adapter;


    Place shippingAddress;

    //Paypal payment
    static PayPalConfiguration config = new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX) // not realy moneyy acc.. just for test
            .clientId(Config.PAYPAL_CLIENT_ID);

    String address,comment;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        //init paypal
        Intent intent = new Intent(this,PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,config);
        startService(intent);

        //FireBase
        database = FirebaseDatabase.getInstance();
        requests = database.getReference("Request");

        //Init
        recyclerView = (RecyclerView)findViewById(R.id.listCart);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        txtTotalPrice = (TextView)findViewById(R.id.total);
        btnPlace = (FButton)findViewById(R.id.btnPlaceOrder);

        btnPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(cart.size()>0)

               showAlertDialog();
                else
                    Toast.makeText(Cart.this, "Košarica je prazna!", Toast.LENGTH_SHORT).show();
            }
        });

        loadListFood();
    }

    private void showAlertDialog() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Cart.this);
        alertDialog.setTitle("Še en korak!");
        alertDialog.setMessage("Vnesite vaš naslov: ");

        LayoutInflater inflater = this.getLayoutInflater();
        View order_address_comment = inflater.inflate(R.layout.order_address_comment,null);

        final MaterialEditText edtAddress = (MaterialEditText)order_address_comment.findViewById(R.id.edtAddress);

        /*PlaceAutocompleteFragment edtAdress = (PlaceAutocompleteFragment)getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        //hide search icon before fragment
        edtAdress.getView().findViewById(R.id.place_autocomplete_search_button).setVisibility(View.GONE);
        //set hint for autocomplete fragment
        ((EditText)edtAdress.getView().findViewById(R.id.place_autocomplete_search_input))
                .setHint("vnesi svoj naslov!");
        //set text size
        ((EditText)edtAdress.getView().findViewById(R.id.place_autocomplete_search_input))
                .setTextSize(14);

        //get address from place autocomplet
        edtAdress.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                shippingAddress = place;
            }

            @Override
            public void onError(Status status) {
                Log.e("NAPAKA",status.getStatusMessage());
            }
        });*/


        final MaterialEditText edtComment = (MaterialEditText)order_address_comment.findViewById(R.id.edtComment);

        final RadioButton rdON_delivery = (RadioButton)order_address_comment.findViewById(R.id.Payment_on_delivery);
        final RadioButton rdPaypal = (RadioButton)order_address_comment.findViewById(R.id.Paypal);

        alertDialog.setView(order_address_comment);

        alertDialog.setIcon(R.drawable.ic_shopping_cart_black_24dp);
        alertDialog.setPositiveButton("DA", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //SHOW paypal to payment


                //first, get address and comment from alert dialog
                address = edtAddress.getText().toString();
                comment = edtComment.getText().toString();





                //Check Payment
                if(!rdON_delivery.isChecked() && !rdPaypal.isChecked())//niti en ni izbran
                {
                    Toast.makeText(Cart.this, "Prosim izberi eno izmed vrst plačila!!", Toast.LENGTH_SHORT).show();


                }
                //check if is not empty
                if(TextUtils.isEmpty(comment) || TextUtils.isEmpty(address)) {
                    Toast.makeText(Cart.this, "Prosim zapolni polja!", Toast.LENGTH_SHORT).show();

                }
                else {

                    if (rdPaypal.isChecked()) {

                        String formatAmount = txtTotalPrice.getText().toString()
                                .replace("$", "")
                                .replace(",", "");

                        float amount = Float.parseFloat(formatAmount);

                        PayPalPayment payPalPayment = new PayPalPayment(new BigDecimal(formatAmount),
                                "USD",
                                "Aplikacija za naročanje hrane",
                                PayPalPayment.PAYMENT_INTENT_SALE);
                        Intent intent = new Intent(getApplicationContext(), PaymentActivity.class);
                        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
                        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payPalPayment);
                        startActivityForResult(intent, PAYPAL_REQUEST_CODE);

                        //remove fragment
                       /* getFragmentManager().beginTransaction()
                                .remove(getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment))
                                .commit();*/

                    } else if (rdON_delivery.isChecked()) {
                        Request request = new Request
                                (Common.currentUser.getPhone(),
                                        Common.currentUser.getName(),
                                        address,
                                        txtTotalPrice.getText().toString(),
                                        "0",
                                        comment,
                                        "Unpaid",
                                        "On Delivery",
                                       // String.format("%s,%s",shippingAddress.getLatLng().latitude,shippingAddress.getLatLng().longitude),
                                        cart);

                        //Submit to FIrebase
                        //we will using system.currentMilli to key
                        requests.child(String.valueOf(System.currentTimeMillis())).setValue(request);

                        //remove fragment
                       /* getFragmentManager().beginTransaction()
                                .remove(getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment))
                                .commit();*/

                        //Delete cart
                        new Database(getBaseContext()).clenCart();
                        Toast.makeText(Cart.this, "Hvala, za podano naročilo", Toast.LENGTH_SHORT).show();
                        finish();


                    }

                }



            }
        });

        alertDialog.setNegativeButton("NE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();

                    //remove fragment
                /*getFragmentManager().beginTransaction()
                        .remove(getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment))
                        .commit();*/

            }
        });
        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == PAYPAL_REQUEST_CODE)
        {
            if(resultCode == RESULT_OK)
            {
                PaymentConfirmation confirmation = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if(confirmation != null)
                {
                    try {

                        String paymentDetail = confirmation.toJSONObject().toString(4);
                        JSONObject jsonObject  = new JSONObject(paymentDetail);


                        //Create new request

                Request request = new Request
                        (Common.currentUser.getPhone(),
                        Common.currentUser.getName(),
                        address,
                        txtTotalPrice.getText().toString(),
                        "0",
                        comment,
                        jsonObject.getJSONObject("response").getString("state"), // state from JSON
                        "PayPal",
                        //String.format("%s,%s",shippingAddress.getLatLng().latitude,shippingAddress.getLatLng().longitude),
                        cart);

                //Submit to FIrebase
                //we will using system.currentMilli to key
                requests.child(String.valueOf(System.currentTimeMillis())).setValue(request);

                //Delete cart
                new Database(getBaseContext()).clenCart();
                Toast.makeText(Cart.this, "Hvala, za podano naročilo", Toast.LENGTH_SHORT).show();
                finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            else if(resultCode == Activity.RESULT_CANCELED)
                Toast.makeText(this, "Plačilo preklicano!", Toast.LENGTH_SHORT).show();
            else if(resultCode == PaymentActivity.RESULT_EXTRAS_INVALID)
                Toast.makeText(this, "Nepravilno plačilo", Toast.LENGTH_SHORT).show();


        }
    }

    private void loadListFood() {


        cart = new Database(this).getCarts();
        adapter = new CartAdapter(cart,this);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);

        //Calculate total price

        int total = 0;
        for(Order order:cart)
            total+=(Integer.parseInt(order.getPrice()))*(Integer.parseInt(order.getQuantity()));
        Locale locale = new Locale("en","US");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);

        txtTotalPrice.setText(fmt.format(total));
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(item.getTitle().equals(Common.DELETE))
            deleteCart(item.getOrder());
        return true;
    }

    private void deleteCart(int order) {

        //remove item from List<Order> by order
        cart.remove(order);
        //delete old data in base
        new Database(this).clenCart();
        //upade new data from list<order> to SQLlite
        for(Order item:cart)
            new Database(this).addToCard(item);

        //Refresh
        loadListFood();
    }
}
