package com.example.daniel.aplikacija2.Remote;

import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofirClient {

    private static Retrofit retrofit = null;

    public  static Retrofit getClient(String baseUrl){

        if(retrofit == null)
        {

            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();

        }

        return retrofit;
    }
}
