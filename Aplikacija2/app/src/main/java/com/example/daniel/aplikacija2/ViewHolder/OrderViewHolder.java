package com.example.daniel.aplikacija2.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.example.daniel.aplikacija2.R;
import com.example.daniel.aplikacija2.Common.Interface.ItemClickListener;

public class OrderViewHolder extends RecyclerView.ViewHolder  {


    public TextView txtOrderId,txtOrderStatus,txtOrderPhone,txtOrderAddress;

    public Button btnEdit,btnRemove,btnDetail,btnDirections;





    public OrderViewHolder(@NonNull View itemView) {
        super(itemView);

        txtOrderAddress = (TextView)itemView.findViewById(R.id.order_address);
        txtOrderId = (TextView)itemView.findViewById(R.id.order_id);
        txtOrderStatus = (TextView)itemView.findViewById(R.id.order_status);
        txtOrderPhone = (TextView)itemView.findViewById(R.id.order_phone);

        btnEdit = (Button)itemView.findViewById(R.id.btnEdit);
        btnDetail = (Button)itemView.findViewById(R.id.btnDetail);
       // btnDirections = (Button)itemView.findViewById(R.id.btnDirections);
        btnRemove = (Button)itemView.findViewById(R.id.btnRemove);








    }

}
