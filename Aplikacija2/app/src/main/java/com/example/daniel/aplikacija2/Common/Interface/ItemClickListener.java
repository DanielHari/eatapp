package com.example.daniel.aplikacija2.Common.Interface;

import android.view.View;

public interface ItemClickListener {

    void OnClick(View view, int position, boolean isLongClick);
}
