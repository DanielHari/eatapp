package com.example.daniel.aplikacija2.Common;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

import com.example.daniel.aplikacija2.Model.Request;
import com.example.daniel.aplikacija2.Model.User;
import com.example.daniel.aplikacija2.Remote.IGeoCordinates;
import com.example.daniel.aplikacija2.Remote.RetrofirClient;

import retrofit2.Retrofit;

public class Common {

    public static User currentUser;
    public static Request currentRequest;


    public static final String UPDATE= "Posodobi";
    public static final String DELETE= "Zbriši";

    public static int PICK_IMAGE_REQUEST= 71;

    public static  final String baseUrl = "https://maps.googleapis.com";

    public static  String ConvertCodeToStatus(String code){

        if(code.equals("0"))
            return "Prejeto naročilo";
        else if(code.equals("1"))
            return "Na poti";
        else
            return "Prispelo";

    }

    public static IGeoCordinates getGeoServices(){

        return RetrofirClient.getClient(baseUrl).create(IGeoCordinates.class);
    }

    public static Bitmap scaleBitmap(Bitmap bitmap, int newWidth,int newHeight){

        Bitmap scaledBitmap = Bitmap.createBitmap(newWidth,newHeight,Bitmap.Config.ARGB_8888);

        float scaleX = newWidth/(float)bitmap.getWidth();
        float scaleY = newHeight/(float)bitmap.getHeight();

        float pivotX=0,pivotY=0;
        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(scaleX,scaleY,pivotX,pivotY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap,0,0,new Paint(Paint.FILTER_BITMAP_FLAG));

        return scaledBitmap;


    }
}
